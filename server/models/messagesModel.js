import mongoose from "mongoose";

const messagesSchema = mongoose.Schema({

    "subject" : String,
    "body" : String,
    "sender" : String,
    "receiver" : String,
    "read" : Boolean
});

const Messages = mongoose.model('messages', messagesSchema);

export default Messages;