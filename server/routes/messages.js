import express from 'express';
import { getMessages, getMessage, addMessage, deleteMessage } from '../controllers/messagesControllers.js'

const router = express.Router();

router.get('/', getMessages);
router.get('/:id', getMessage);
router.put('/', addMessage);
router.delete('/:id', deleteMessage);

export default router;