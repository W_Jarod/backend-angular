import Messages from "../models/messagesModel.js";

export const getMessages = async (req, res) => 
{
    try
    {
        const messages = await Messages.find()
        res.status(200).json(messages);
    }catch (error)
    {
        res.status(404).json({ message: error.message});
    }
}

export const getMessage = async (req, res) => {
    try
    {
        const upOffre = await Offres.findById(req.params.id);
        res.status(201).json(upOffre);
        console.log(req.params.id)
    }catch (error) 
    {
        res.status(404).json({ message: error.message});
    }
}

export const addMessage = async (req, res) => 
{
    const body = req.body;
    const newMessage = new Messages(body);

    try {

        await newMessage.save();
        res.status(201).json(newMessage);        
    } catch (error) {

        console.log(error);
        res.status(404).json({ message: error.message});
        
    }
}


export const deleteMessage = async (req, res) => {
    try
    {
        await Offres.findByIdAndDelete(req.params.id);
        res.status(201).json(req.params.id+' : supprimer');
        console.log(req.params.id+' : supprimer')
    }catch (error) 
    {
        res.status(404).json({ message: error.message});
    }
}